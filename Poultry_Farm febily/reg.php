<?php
session_start();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Poultry Farm</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="css/style.css">
    <script src="js/validation.js"></script>"
</head>
<body>

    <div class="main">

        <section class="signup">
            <!-- <img src="images/signup-bg.jpg" alt=""> -->
            <div class="container">
                <div class="signup-content">
                    <form method="POST" id="reg" name="myForm" class="signup-form" action="regpost.php">
                        <h2 class="form-title">Create account</h2>
                        <div class="form-group">
                            <input type="text" class="form-input" name="name" id="name" placeholder="Your Name" onblur="full_name(this.id)" required/>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-input" name="pin" id="pin" placeholder="Your pincode" required/>
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-input" name="phno" id="phno" placeholder="Your Phone Number" onblur="phone_no(this.id)" required/>
                        </div>
                        <div class="form-group">
                            <select type="text" class="form-input" name="user" id="user" required>
                                <option value="NULL" disabled selected>Choose one</option>
            <?php
            require('dbconnection.php');
            $query = "select * from tbl_type";
            $result = mysqli_query($con, $query);
            mysqli_close($con);
            if (!$result){
              die("QUERY die");
            } else {
              while ($row = mysqli_fetch_array($result)){
                echo "<option value=\"".$row['type_id']."\">".$row['type_name']."</option>";
              }
            }
            ?>
                              </select>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-input" name="email" id="email" placeholder="Your Email" onblur="email_id(this.id)"/>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-input" name="password" id="passone" placeholder="Password" />
                            <!-- onblur="pass(this.id)"<span toggle="#password" class="zmdi zmdi-eye field-icon toggle-password"></span> -->
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-input" name="re_password" id="passtwo" placeholder="Repeat your password" />
                            <!-- onblur="con_pass(this.id)" -->
                        </div>
                        <div class="form-group">
                            <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" />
                            <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree all statements in  <a href="#" class="term-service">Terms of service</a></label>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" id="submit" class="form-submit" value="Sign up" onclick="val('reg')"/>
                        </div>
                    </form>
                    <p class="loginhere">
                        Have already an account ? <a href="Login.html" class="loginhere-link">Login here</a>
                    </p>
                </div>
            </div>
        </section>

    </div>

    <!-- JS -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="js/main.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>