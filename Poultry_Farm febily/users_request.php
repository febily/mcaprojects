<?php
session_start();
if(!isset($_SESSION['id'])){
  header('login.php');
}
if ($_SESSION["role"]!="admin")
 {
  header("Location: index.html");
}?>
<style>
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
}
.container1
{
  padding: 50px;
}



</style>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Poultry Farm</title>
<!-- 
Cafe House Template
http://www.templatemo.com/tm-466-cafe-house
-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
  <link href="css/bootstraps.min.css" rel="stylesheet">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/templatemo-style.css" rel="stylesheet">
  <!-- <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" /> -->


  </head>
  <body>
    <!-- Preloader -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <!-- End Preloader -->
    <div class="tm-top-header">
      <div class="container">
        <div class="row">
          <div class="tm-top-header-inner">
            <div class="tm-logo-container">
              <!-- <img src="img/logo.png" alt="Logo" class="tm-site-logo"> -->
              <h1 class="tm-site-name tm-handwriting-font">Poultry Farm</h1>
            </div>
            <div class="mobile-menu-icon">
              <i class="fa fa-bars"></i>
            </div>
            <nav class="tm-nav">
              <ul>
                <li><a href="admin_index.php" class="active">Home</a></li>
                <li><a href="report1.php" class="active">Report</a></li>
                 <li><a href="edit_pass.php" class="active">Edit password</a></li>
               <li><a href="Logout.php">logout</a></li>
              </ul>
            </nav>   
          </div>           
        </div>    
      </div>
    </div>
    <section class="tm-welcome-section" style="padding:10px;">
    <div  style="text-align: center; padding-left:50px; color:black">
      <h1 style="color: white;padding-top: 30px; ">USER REQUESTS</h1>
  <?php
  include("dbconnection.php");
  $sql1="select * from `tbl_reg` where status=1";
  $res1=mysqli_query($con,$sql1);
    $n=mysqli_num_rows($res1);
if($n==0)
{

  echo "<div  style=' padding:100px; color:white; font-size:60px;'>NO PENDING REQUESTS</div>";
}
else
{
  echo "<table class='table table-responsive' id='tbl' class='table' style='display:block; padding:50px; color:white; font-size:20px;padding-left:250px;'>";
  echo "<tr>";
  echo"<th> Name</th>";
  echo"<th>Location pincode</th>";
  echo"<th>User type</th>";
  echo"<th>E-mail</th>";
  echo"<th>Phone</th>";
  echo"<th></th>";
  echo"<th></th>";
  echo"</tr>";
  while($row=mysqli_fetch_array($res1))
  {
     $login_id=$row['login_id'];
  echo"<tr >";
  echo"<td>",$row['name'],"</td><td>&nbsp;",$row['location'],"</td>";
  $type_id=$row['type_id'];
  $sql2="select `type_name` from `tbl_type` where `type_id`='$type_id'";
  $res2=mysqli_query($con,$sql2);

  while($row2=mysqli_fetch_array($res2))
  {
     echo "<td>&nbsp;",$row2['type_name'],"</td>";
  }

  echo "<td>&nbsp;",$row['email'],"</td><td>&nbsp;",$row['phoneno'],"</td>";?>
  <td><a href='approveuser.php?id=<?php echo $login_id?>' class='button button1'>Approve</a></td>
  <td><a href='deleteuser.php?id=<?php echo $login_id?>' class='button button1'>Delete</a></td>
 <?php echo"</tr>";

  }
  echo"  </table>";
 } ?>
</div>

    </section>
  <section></section>
    <footer>
        <div class="container">
          <div class="row tm-copyright">
           <p class="col-lg-12 small copyright-text text-center">Copyright &copy; 2084 Your Cafe House</p>
         </div>  
       </div>
     </div>
   </footer> <!-- Footer content-->  
   <!-- JS -->
   <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>      <!-- jQuery -->
   <script type="text/javascript" src="js/templatemo-script.js"></script>      <!-- Templatemo Script -->

 </body>
 </html>
