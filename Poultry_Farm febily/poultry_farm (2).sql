-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2021 at 06:47 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `poultry_farm`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `cid` int(11) NOT NULL,
  `cname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`cid`, `cname`) VALUES
(1, 'pre-starter'),
(2, 'starter'),
(3, 'Finisher'),
(4, 'pre-starter2');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chickrate`
--

CREATE TABLE `tbl_chickrate` (
  `rate id` int(11) NOT NULL,
  `rate_date` date NOT NULL,
  `t_rate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_chickrate`
--

INSERT INTO `tbl_chickrate` (`rate id`, `rate_date`, `t_rate`) VALUES
(1, '2021-06-01', 99),
(2, '2021-06-03', 99),
(3, '2021-06-04', 92),
(4, '2021-06-14', 75),
(5, '2021-06-24', 70),
(6, '2021-06-28', 70);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_farmer_bill`
--

CREATE TABLE `tbl_farmer_bill` (
  `fbill_id` int(11) NOT NULL,
  `forder_id` int(11) NOT NULL,
  `fbill_date` date NOT NULL,
  `fprice` int(11) NOT NULL,
  `fstatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_farmer_bill`
--

INSERT INTO `tbl_farmer_bill` (`fbill_id`, `forder_id`, `fbill_date`, `fprice`, `fstatus`) VALUES
(202100, 5, '2021-06-04', 7500, 1),
(202101, 6, '2021-06-04', 700, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_farmer_birds`
--

CREATE TABLE `tbl_farmer_birds` (
  `fbird_id` int(11) NOT NULL,
  `fbird_count` int(11) NOT NULL,
  `fbird_bdate` date NOT NULL,
  `fbird_weight` float NOT NULL,
  `login_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_farmer_birds`
--

INSERT INTO `tbl_farmer_birds` (`fbird_id`, `fbird_count`, `fbird_bdate`, `fbird_weight`, `login_id`) VALUES
(3, 100, '2021-06-02', 1.75, 11),
(4, 100, '2021-06-04', 1.8, 11),
(5, 200, '2021-06-11', 1.59, 14);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hatchery_bill`
--

CREATE TABLE `tbl_hatchery_bill` (
  `hbill_id` int(11) NOT NULL,
  `horder_id` int(11) NOT NULL,
  `hbill_date` date NOT NULL,
  `hprice` int(11) NOT NULL,
  `hstatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_hatchery_bill`
--

INSERT INTO `tbl_hatchery_bill` (`hbill_id`, `horder_id`, `hbill_date`, `hprice`, `hstatus`) VALUES
(500051, 1, '2021-06-04', 700, 1),
(500052, 2, '2021-06-04', 10850, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hatchery_birds`
--

CREATE TABLE `tbl_hatchery_birds` (
  `hbird_id` int(11) NOT NULL,
  `hbird_count` int(11) NOT NULL,
  `hbird_bdate` date NOT NULL,
  `hbird_weight` float NOT NULL,
  `login_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_hatchery_birds`
--

INSERT INTO `tbl_hatchery_birds` (`hbird_id`, `hbird_count`, `hbird_bdate`, `hbird_weight`, `login_id`) VALUES
(1, 170, '2021-06-09', 2.4, 9);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE `tbl_login` (
  `login_id` int(11) NOT NULL,
  `role` varchar(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`login_id`, `role`, `username`, `password`, `status`) VALUES
(1, 'admin', 'admin', 'admin1', 2),
(9, '4', 'sachu', 'sachu', 2),
(11, '1', 'febily', 'febily', 2),
(12, '2', 'faina', 'faina1', 2),
(13, '2', 'appu', 'appu', 2),
(14, '1', 'dora', 'dora', 2),
(15, '3', 'beena ', 'beena', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_birds_hatchery`
--

CREATE TABLE `tbl_order_birds_hatchery` (
  `horder_id` int(11) NOT NULL,
  `hhatchery_id` int(11) NOT NULL,
  `hcount` int(11) NOT NULL,
  `hddate` date NOT NULL,
  `horder_date` date NOT NULL,
  `haddress` varchar(100) NOT NULL,
  `login_id` int(11) NOT NULL,
  `hstatus` int(11) NOT NULL,
  `hbird_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_order_birds_hatchery`
--

INSERT INTO `tbl_order_birds_hatchery` (`horder_id`, `hhatchery_id`, `hcount`, `hddate`, `horder_date`, `haddress`, `login_id`, `hstatus`, `hbird_id`) VALUES
(1, 6, 10, '2021-06-28', '2021-06-03', 'mkj', 11, 3, 1),
(2, 6, 155, '2021-06-24', '2021-06-03', 'jkhj', 11, 3, 1),
(3, 6, 141, '2021-06-19', '2021-06-03', 'mmmmmmmmmmmmmmm', 11, 1, 1),
(4, 6, 150, '2021-06-23', '2021-06-04', 'xgfxg', 11, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_birds_wholesalers`
--

CREATE TABLE `tbl_order_birds_wholesalers` (
  `worder_id` int(11) NOT NULL,
  `wfarmer_id` int(11) NOT NULL,
  `wcount` int(11) NOT NULL,
  `wddate` date NOT NULL,
  `worder_date` date NOT NULL,
  `waddress` varchar(100) NOT NULL,
  `login_id` int(11) NOT NULL,
  `wstatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_order_birds_wholesalers`
--

INSERT INTO `tbl_order_birds_wholesalers` (`worder_id`, `wfarmer_id`, `wcount`, `wddate`, `worder_date`, `waddress`, `login_id`, `wstatus`) VALUES
(5, 7, 100, '2021-06-14', '2021-06-03', 'Sparksuite, Inc. ERNAKULAM\r\n\r\n12345 Sunny Road', 15, 3),
(6, 7, 10, '2021-06-24', '2021-06-03', 'sszc\r\nsdczxc', 15, 3),
(7, 7, 10, '2021-06-24', '2021-06-03', 'sszc\r\nsdczxc', 15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_food_suplier`
--

CREATE TABLE `tbl_order_food_suplier` (
  `sorder_id` int(11) NOT NULL,
  `ssuplier_id` int(11) NOT NULL,
  `stype_id` int(11) NOT NULL,
  `squantity` int(11) NOT NULL,
  `sddate` date NOT NULL,
  `sorder_date` date NOT NULL,
  `saddress` varchar(100) NOT NULL,
  `login_id` int(11) NOT NULL,
  `sstatus` int(11) NOT NULL,
  `sfood_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_order_food_suplier`
--

INSERT INTO `tbl_order_food_suplier` (`sorder_id`, `ssuplier_id`, `stype_id`, `squantity`, `sddate`, `sorder_date`, `saddress`, `login_id`, `sstatus`, `sfood_id`) VALUES
(1007, 9, 3, 10, '2021-06-18', '2021-06-04', 'aad', 11, 3, 6),
(1010, 9, 3, 100, '2021-06-23', '2021-06-04', 'dfyhgf', 11, 1, 2),
(1011, 8, 1, 10, '2021-06-16', '2021-06-04', 'gvhgf', 11, 2, 4),
(1012, 8, 1, 1000, '2021-06-16', '2021-06-04', 'hjkjk', 11, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reg`
--

CREATE TABLE `tbl_reg` (
  `reg_id` int(11) NOT NULL,
  `login_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `location` int(11) NOT NULL,
  `type_id` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phoneno` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_reg`
--

INSERT INTO `tbl_reg` (`reg_id`, `login_id`, `name`, `location`, `type_id`, `email`, `phoneno`, `status`) VALUES
(6, 9, 'sachu', 682022, '4', 'admin@gmail.com', '9946777777', 2),
(7, 11, 'febily', 682504, '1', 'febzfranklin@gmail.com', '9497418661', 2),
(8, 12, 'faina', 682504, '2', 'fainamaymol2416@gmail.com', '9497418661', 2),
(9, 13, 'appu', 682540, '2', 'appu@gmail.com', '9497418661', 2),
(10, 14, 'dora', 682508, '1', 'dora@gmail.com', '9497418661', 2),
(11, 15, 'beena ', 682022, '3', 'beena@gmail.com', '9497418661', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_suplier_bill`
--

CREATE TABLE `tbl_suplier_bill` (
  `sbill_id` int(11) NOT NULL,
  `sorder_id` int(11) NOT NULL,
  `sbill_date` date NOT NULL,
  `sprice` int(11) NOT NULL,
  `sstatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_suplier_bill`
--

INSERT INTO `tbl_suplier_bill` (`sbill_id`, `sorder_id`, `sbill_date`, `sprice`, `sstatus`) VALUES
(20115, 1007, '2021-06-04', 1500, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_suplier_food`
--

CREATE TABLE `tbl_suplier_food` (
  `sfood_id` int(11) NOT NULL,
  `sfood_type` int(11) NOT NULL,
  `sfood_quantity` float NOT NULL,
  `sfood_edate` date NOT NULL,
  `sfood_price` float NOT NULL,
  `sfood_status` int(11) NOT NULL,
  `login_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_suplier_food`
--

INSERT INTO `tbl_suplier_food` (`sfood_id`, `sfood_type`, `sfood_quantity`, `sfood_edate`, `sfood_price`, `sfood_status`, `login_id`) VALUES
(1, 2, 30, '2021-06-24', 500, 1, 12),
(2, 3, 290, '2021-06-26', 150, 1, 13),
(3, 1, 120, '2021-06-27', 100, 1, 12),
(4, 1, 100, '2021-06-29', 100, 1, 12),
(6, 3, 90, '2021-06-25', 50, 1, 12);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_type`
--

CREATE TABLE `tbl_type` (
  `type_id` int(11) NOT NULL,
  `type_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_type`
--

INSERT INTO `tbl_type` (`type_id`, `type_name`) VALUES
(1, 'Farmer'),
(2, 'Food Supplier'),
(3, 'Wholesaler'),
(4, 'Hatchery');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `tbl_chickrate`
--
ALTER TABLE `tbl_chickrate`
  ADD PRIMARY KEY (`rate id`);

--
-- Indexes for table `tbl_farmer_bill`
--
ALTER TABLE `tbl_farmer_bill`
  ADD PRIMARY KEY (`fbill_id`);

--
-- Indexes for table `tbl_farmer_birds`
--
ALTER TABLE `tbl_farmer_birds`
  ADD PRIMARY KEY (`fbird_id`);

--
-- Indexes for table `tbl_hatchery_bill`
--
ALTER TABLE `tbl_hatchery_bill`
  ADD PRIMARY KEY (`hbill_id`);

--
-- Indexes for table `tbl_hatchery_birds`
--
ALTER TABLE `tbl_hatchery_birds`
  ADD PRIMARY KEY (`hbird_id`);

--
-- Indexes for table `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`login_id`);

--
-- Indexes for table `tbl_order_birds_hatchery`
--
ALTER TABLE `tbl_order_birds_hatchery`
  ADD PRIMARY KEY (`horder_id`);

--
-- Indexes for table `tbl_order_birds_wholesalers`
--
ALTER TABLE `tbl_order_birds_wholesalers`
  ADD PRIMARY KEY (`worder_id`);

--
-- Indexes for table `tbl_order_food_suplier`
--
ALTER TABLE `tbl_order_food_suplier`
  ADD PRIMARY KEY (`sorder_id`);

--
-- Indexes for table `tbl_reg`
--
ALTER TABLE `tbl_reg`
  ADD PRIMARY KEY (`reg_id`);

--
-- Indexes for table `tbl_suplier_bill`
--
ALTER TABLE `tbl_suplier_bill`
  ADD PRIMARY KEY (`sbill_id`);

--
-- Indexes for table `tbl_suplier_food`
--
ALTER TABLE `tbl_suplier_food`
  ADD PRIMARY KEY (`sfood_id`);

--
-- Indexes for table `tbl_type`
--
ALTER TABLE `tbl_type`
  ADD PRIMARY KEY (`type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_chickrate`
--
ALTER TABLE `tbl_chickrate`
  MODIFY `rate id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_farmer_bill`
--
ALTER TABLE `tbl_farmer_bill`
  MODIFY `fbill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202102;

--
-- AUTO_INCREMENT for table `tbl_farmer_birds`
--
ALTER TABLE `tbl_farmer_birds`
  MODIFY `fbird_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_hatchery_bill`
--
ALTER TABLE `tbl_hatchery_bill`
  MODIFY `hbill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=500053;

--
-- AUTO_INCREMENT for table `tbl_hatchery_birds`
--
ALTER TABLE `tbl_hatchery_birds`
  MODIFY `hbird_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_login`
--
ALTER TABLE `tbl_login`
  MODIFY `login_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_order_birds_hatchery`
--
ALTER TABLE `tbl_order_birds_hatchery`
  MODIFY `horder_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_order_birds_wholesalers`
--
ALTER TABLE `tbl_order_birds_wholesalers`
  MODIFY `worder_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_order_food_suplier`
--
ALTER TABLE `tbl_order_food_suplier`
  MODIFY `sorder_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1013;

--
-- AUTO_INCREMENT for table `tbl_reg`
--
ALTER TABLE `tbl_reg`
  MODIFY `reg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_suplier_bill`
--
ALTER TABLE `tbl_suplier_bill`
  MODIFY `sbill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20116;

--
-- AUTO_INCREMENT for table `tbl_suplier_food`
--
ALTER TABLE `tbl_suplier_food`
  MODIFY `sfood_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_type`
--
ALTER TABLE `tbl_type`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
