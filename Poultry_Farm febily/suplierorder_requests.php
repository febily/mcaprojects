<?php
session_start();
if(!isset($_SESSION['id'])){
  header('login.php');
}
if ($_SESSION["role"]!=2)
 {
  header("Location: index.html");
}?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Poultry Farm</title>
<!-- 
Cafe House Template
http://www.templatemo.com/tm-466-cafe-house
-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
  <link href="css/bootstraps.min.css" rel="stylesheet">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/templatemo-style.css" rel="stylesheet">
  <!-- <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" /> -->
<style type="text/css">
  .button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 10px 20px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 15px;
  margin: 4px 2px;
  cursor: pointer;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
}
 .button1 {
background-color: #cc0000; /* Green */
  border: none;
  color: white;
  padding: 10px 20px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 15px;
  margin: 4px 2px;
  cursor: pointer;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
}



  input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
  width: 100%;
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

#cat{
  width: 600px;
    margin: auto;
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}
.tm-main-section { padding: 100px; padding-left: 150px; }
div.main {
  width: 100px;
  margin: auto;
}
#cont
{
  padding:50px;
  font-weight: bolder;
  color: white;
}
</style>

  </head>
  <body>
    <!-- Preloader -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <!-- End Preloader -->
    <div class="tm-top-header">
      <div class="container">
        <div class="row">
          <div class="tm-top-header-inner">
            <div class="tm-logo-container">
              <!-- <img src="img/logo.png" alt="Logo" class="tm-site-logo"> -->
              <h1 class="tm-site-name tm-handwriting-font">Poultry Farm</h1>
            </div>
            <div class="mobile-menu-icon">
              <i class="fa fa-bars"></i>
            </div>
            <nav class="tm-nav">
              <ul>
               <li><a href="suplier_index.php" class="active">Home</a></li>
                <li><a href="suplierview_stock.php">View Stock</a></li>
                <li><a href="edit_pass.php">Edit Password</a></li>
                  <li><a href="logout.php">Logout</a></li>
              </ul>
            </nav>   
          </div>           
        </div>    
      </div>
    </div>
 
          <section class="tm-welcome-section" style="padding: 100px;">
    <div class="" style="text-align: center; padding-left:50px; color:black">
      <h1 style="color: white;padding-top: 30px; "> ORDERS FROM FARMERS</h1>

  <?php

  include("dbconnection.php");
  $login=$_SESSION['id'];
  
  $sql1="SELECT sorder_id, name, cname,cid, squantity, sddate, sorder_date, saddress, sstatus FROM `tbl_order_food_suplier` s, tbl_reg r , tbl_category c WHERE s.login_id=r.login_id AND c.cid=s.stype_id AND sstatus=0;";
  $res1=mysqli_query($con,$sql1);
  $n=mysqli_num_rows($res1);
if($n==0)
{
  echo "<div class='container' id='cont'><h1>No Order Requests</h1></div>";
}
else
{
  echo "<table class='table table-responsive' id='tbl' class='table' style='display:block; padding:50px;padding-left:50px; color:white; font-size:20px;'>";
  echo "<tr>";
  echo"<th> SUPLIER NAME</th>";
  echo"<th>FOOD TYPE</th>";
  echo"<th>QUANTITY</th>";
echo"<th>DELIVERY DATE</th>";
echo"<th>ORDERED DATE</th>";
echo"<th>ADDRESS</th>";
echo"<th></th>";
echo"<th></th>";
  echo"</tr>";
  while($row=mysqli_fetch_array($res1))
  {
     
  echo"<tr >";
  echo"<td>",$row['name'],"</td><td>&nbsp;",$row['cname'],"</td>";
  
     echo "<td>&nbsp;",$row['squantity'],"</td>";
        echo "<td>&nbsp;",$row['sddate'],"</td>";
           echo "<td>&nbsp;",$row['sorder_date'],"</td>";
              echo "<td>&nbsp;",$row['saddress'],"</td>";
     $id=$row['sorder_id'];
     $_SESSION['cid']=$row['cid'];
       echo "<td><a href='suplierconfirm_order.php?id=$id' class='button'>Confirm Order</a>
       </td><td>
       <a href='suplierreject_order.php?id=$id' class='button1'>Reject Order</a></td>";
     
     

     
  ?>
   
 <?php echo"</tr>";

  }
  echo"  </table>";
}
  ?>
</div>

    </section>
         





       
    <div style="padding: 50x;"></div>
    <footer>
           
      <div>
        <div class="container">
          <div class="row tm-copyright">
           <p class="col-lg-12 small copyright-text text-center">Copyright &copy; 2021 poulry farm</p>
         </div>  
       </div>
     </div>
   </footer> <!-- Footer content-->  
   <!-- JS -->
   <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>      <!-- jQuery -->
   <script type="text/javascript" src="js/templatemo-script.js"></script>      <!-- Templatemo Script -->

 </body>
 </html>