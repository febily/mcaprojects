</!DOCTYPE html>
<html>
<head>
	<!-- pdf download -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
<!-- pdf download -->
<style type="text/css">
	
 .invoice-box {
                max-width: 800px;
                margin: auto;
                padding: 30px;
                border: 1px solid #eee;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
                font-size: 16px;
                line-height: 24px;
                font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                color: #555;
            }

            .invoice-box table {
                width: 100%;
                line-height: inherit;
                text-align: left;
            }

            .invoice-box table td {
                padding: 5px;
                vertical-align: top;
            }

            .invoice-box table tr td:nth-child(2) {
                text-align: right;
            }

            .invoice-box table tr.top table td {
                padding-bottom: 20px;
            }

            .invoice-box table tr.top table td.title {
                font-size: 45px;
                line-height: 45px;
                color: #333;
            }

            .invoice-box table tr.information table td {
                padding-bottom: 40px;
            }

            .invoice-box table tr.heading td {
                background: #eee;
                border-bottom: 1px solid #ddd;
                font-weight: bold;
            }

            .invoice-box table tr.details td {
                padding-bottom: 20px;
            }

            .invoice-box table tr.item td {
                border-bottom: 1px solid #eee;
            }

            .invoice-box table tr.item.last td {
                border-bottom: none;
            }

            .invoice-box table tr.total td:nth-child(2) {
                border-top: 2px solid #eee;
                font-weight: bold;
            }

            @media only screen and (max-width: 600px) {
                .invoice-box table tr.top table td {
                    width: 100%;
                    display: block;
                    text-align: center;
                }

                .invoice-box table tr.information table td {
                    width: 100%;
                    display: block;
                    text-align: center;
                }
            }

            /** RTL **/
            .invoice-box.rtl {
                direction: rtl;
                font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            }

            .invoice-box.rtl table {
                text-align: right;
            }

            .invoice-box.rtl table tr td:nth-child(2) {
                text-align: left;
            }
</style>

</head>
<body>
	<section class="tm-welcome-section" style="padding: 50px;">
<div class="" style="text-align: center; padding-left:50px; color:white; ">
      <h1 style="color: white;padding-top: 30px;padding-bottom: 10px; ">YOUR BILL</h1>

  <?php

  include("dbconnection.php");
  $login=$_SESSION['id'];
   // echo $login;
   // 
   $order_id = $_GET['id'];
  $sql1="SELECT fbill_id,worder_date,wddate,waddress,name,email, phoneno, wcount,t_rate,fprice, w.login_id as login FROM `tbl_order_birds_wholesalers` w, tbl_reg r , `tbl_farmer_bill` b, `tbl_chickrate` c WHERE w.wfarmer_id=r.reg_id AND w.worder_id=b.forder_id AND c.rate_date=w.wddate AND forder_id=$order_id;";
  $res1=mysqli_query($con,$sql1);
while($row=mysqli_fetch_array($res1))
  {


?>
      <div class="invoice-box" style="background-color: white" id="d1">
            <table cellpadding="0" cellspacing="0">
                <tr class="top">
                    <td colspan="2">
                        <table>
                            <tr>
                                <td class="title">
                                     <h1 class="tm-site-name tm-handwriting-font" style="font-size: 50px;">Poultry Farm</h1>
                                </td>

                                <td>
                                    Invoice #: <?php echo $row['fbill_id'];?><br />
                                    Order Dare: <?php echo $row['worder_date'];?><br />
                                    Delevered Date: <?php echo $row['wddate'];?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr class="information">
                    <td colspan="2">
                        <table>
                            <tr>
                                <td>
                                  <?php
                                  $login=$row['login'];
                                    $s="SELECT * FROM `tbl_reg` where login_id=$login";
                                     $r1=mysqli_query($con,$s);
                                    while($row1=mysqli_fetch_array($r1))
                                      {
                                
                                    echo "",$row1['name'],"<br />";
                                    echo "",$row['waddress'],"<br />";
                                    echo "",$row1['email'],"<br />";
                                    echo "",$row1['phoneno'],"<br />";
                                    }
                                    ?>
                                </td>

                                <td>
                                    <?php echo $row['name'];?><br />
                                    <?php echo $row['email'];?><br />
                                   <?php echo $row['phoneno'];?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>


                <tr class="heading">
                    <td>Item</td>

                    <td>#</td>
                </tr>

                <tr class="item">
                    <td>Birds (Count)</td>

                    <td> <?php echo $row['wcount'];?></td>
                </tr>

                <tr class="item">
                    <td>Price (Per Bird)</td>

                    <td> <?php echo $row['t_rate'];?></td>
                </tr>

                

                <tr class="total">
                    <td></td>

                    <td>Total: <?php echo $row['fprice'];?></td>
                </tr>
            </table>
        </div>
        <div style="padding: 20px;">
  <center><button class="button" id="btnExport" ><i class="fa fa-download"></i> Download</button></center>

</div>
</div>
<?php }?>
    </section>
</body>


</html>
 <script type="text/javascript">
        $("body").on("click", "#btnExport", function () {
            html2canvas($('#d1')[0], {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("bill.pdf");
                }
            });
        });
    </script>