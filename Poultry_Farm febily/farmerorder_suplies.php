<?php
session_start();
if(!isset($_SESSION['id'])){
  header('login.php');
}
if ($_SESSION["role"]!=1)
 {
  header("Location: index.html");
}include("dbconnection.php");?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Poultry Farm</title>
<!-- 
Cafe House Template
http://www.templatemo.com/tm-466-cafe-house
-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
  <link href="css/bootstraps.min.css" rel="stylesheet">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/templatemo-style.css" rel="stylesheet">
  <!-- <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" /> -->
<style type="text/css">
  .button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
}



  input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
  width: 100%;
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

#cat{
  width: 600px;
    margin: auto;
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}
.tm-main-section { padding: 100px; padding-left: 150px; }
div.main {
  width: 100px;
  margin: auto;
}
</style>

  </head>
  <body>
    <!-- Preloader -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <!-- End Preloader -->
    <div class="tm-top-header">
      <div class="container">
        <div class="row">
          <div class="tm-top-header-inner">
            <div class="tm-logo-container">
              <!-- <img src="img/logo.png" alt="Logo" class="tm-site-logo"> -->
              <h1 class="tm-site-name tm-handwriting-font">Poultry Farm</h1>
            </div>
            <div class="mobile-menu-icon">
              <i class="fa fa-bars"></i>
            </div>
            <nav class="tm-nav">
              <ul>
                <li><a href="farmer_index.php" class="active">Home</a></li>
                <!-- <li><a href="report1.php" class="active">Report</a></li> -->
                <li><a href="farmerview_stock.php">View Stock</a></li>
                 <li><a href="edit_pass.php" class="active">Edit password</a></li>
               <li><a href="Logout.php">logout</a></li>
              </ul>
            </nav>   
          </div>           
        </div>    
      </div>
    </div>
   
    <div class="tm-main-section light-gray-bg">
      <div style="margin: auto;padding-left: 300px;padding-bottom: 50px;">
           <a href="farmeradd_bird.php" class="button button1">Add Birds</a>
           <a href="farmerview_order.php" class="button button1">View Orders</a>
          <a href="farmerorder_suplies.php" class="button button1">Order Suplies</a>
           <a href="farmerorder_birds.php" class="button button1">Order Birds</a>
          
          </div>
      <div class="container" id="main">
         
          <div id="cat">
            <form action="ordersupliesfarmer.php" method="post">
              <CENTER><h3>ORDER FOOD SUPLIES</h3></CENTER>
               <label for="count">Select Shop</label>
             <?php
                $sql1="SELECT reg_id, name FROM tbl_reg WHERE type_id=2;";
                $res1=mysqli_query($con,$sql1);
                ?>
              <select class="form-control input-lg" name="suplier" id="suplier">
                <?php
                 while($row=mysqli_fetch_array($res1))
                {
                  ?>
                  <option value="<?php echo $row['reg_id'];?>"><?php echo $row['name']; ?></option>
                  <?php
                $id=$row['reg_id'];
                }
                ?>
              </select>
             <label for="count">Select Batch of Expiry Date</label>
             <?php
                $sql1="SELECT sfood_id, sfood_edate FROM `tbl_suplier_food` ;";
                $res1=mysqli_query($con,$sql1);
                ?>
              <select class="form-control input-lg" name="foodid" id="foodid">
                <?php
                 while($row=mysqli_fetch_array($res1))
                {
                  ?>
                  <option value="<?php echo $row['food_id'];?>"><?php echo $row['sfood_edate']; ?></option>
                  <?php
                // $id=$row['reg_id'];
                }
                ?>
              </select> 
              <label for="count">Select Food Type</label>
             <?php
                $sql1="select sfood_id,cid,cname FROM `tbl_category` c, `tbl_suplier_food` s WHERE c.cid= s.sfood_type ;";
                $res1=mysqli_query($con,$sql1);
                ?>
             
              <select class="form-control input-lg" name="type" id="type" >
                <?php
                 while($row=mysqli_fetch_array($res1))
                {
                  ?>
                  <option value="<?php echo $row['sfood_id'];?>"><?php echo $row['cname']; ?></option>
                  <?php
                }
                ?>
              </select>
              
            <label for="quantity">Food Quantity</label>

              <input type="number" class="form-control input-lg" id="quantity" name="quantity" placeholder="Enter Quantity " required="please">

              <label for="ddate">Date of Delivery</label>

              <input class="form-control input-lg" type="date" id="ddate" name="ddate" placeholder="Date of Delivery of order " required="please">
              <label for="weight">Delivery Address</label>

             <textarea   class="form-control input-lg" id="address" name="address" placeholder="Enter address to Deliver" required="please enter address"></textarea>

              <input type="submit" value="Order">
              
            </form>
          </div>
      </div>
    </div> 
    <div style="padding: 50x;"></div>
    <footer>
           
      <div>
        <div class="container">
          <div class="row tm-copyright">
           <p class="col-lg-12 small copyright-text text-center">Copyright &copy; 2021 poulry farm</p>
         </div>  
       </div>
     </div>
   </footer> <!-- Footer content-->  
   <!-- JS -->
   <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>      <!-- jQuery -->
   <script type="text/javascript" src="js/templatemo-script.js"></script>      <!-- Templatemo Script -->

 </body>
 </html>