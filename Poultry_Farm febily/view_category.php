<?php
session_start();
if(!isset($_SESSION['id'])){
  header('login.php');
}
if ($_SESSION["role"]!="admin")
 {
  header("Location: index.html");
}
include("dbconnection.php");?>

<style>

* {
  box-sizing: border-box;
}

/* Create two equal columns that floats next to each other */
.column {
  float: left;
  width: 50%;
  padding: 10px;
  height: 300px; /* Should be removed. Only for demonstration */
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}



</style>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Poultry Farm</title>
<!-- 
Cafe House Template
http://www.templatemo.com/tm-466-cafe-house
-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
  <link href="css/bootstraps.min.css" rel="stylesheet">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/templatemo-style.css" rel="stylesheet">
  <!-- <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" /> -->


  </head>
  <body>
    <!-- Preloader -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <!-- End Preloader -->
    <div class="tm-top-header">
      <div class="container">
        <div class="row">
          <div class="tm-top-header-inner">
            <div class="tm-logo-container">
              <!-- <img src="img/logo.png" alt="Logo" class="tm-site-logo"> -->
              <h1 class="tm-site-name tm-handwriting-font">Poultry Farm</h1>
            </div>
            <div class="mobile-menu-icon">
              <i class="fa fa-bars"></i>
            </div>
            <nav class="tm-nav">
              <ul>
                <li><a href="admin_index.php" class="active">Home</a></li>
                <li><a href="report1.php" class="active">Report</a></li>
                 <li><a href="edit_pass.php" class="active">Edit password</a></li>
               <li><a href="Logout.php">logout</a></li>
              </ul>
            </nav>   
          </div>           
        </div>    
      </div>
    </div>

  
            <section class="tm-welcome-section" style="padding-top:  10px;">
              <h1 style="color: white;padding-top: 30px; ">USER CATEGORY</h1>
              <div class="container" style="text-align: center; padding-left:200px; color:black">
                
                <?php
                $sql1="select * from `tbl_type`";
                $res1=mysqli_query($con,$sql1);
                echo "<table class='table table-responsive' id='tbl' class='table'style='display:block; padding:50px; color:white; font-size:20px;'>";
                echo "<tr>";
                echo"<th> USER CATEGORY ID</th>";
                echo"<th> USER CATEGORY NAME</th>";
                echo"</tr>";
                while($row=mysqli_fetch_array($res1))
                {
                echo"<tr >";
                echo"<td>",$row['type_id'],"</td><td>&nbsp;",$row['type_name'],"</td>";
                echo"</tr>";
                }
                echo"  </table>";
                ?>
              
              </div>
            </section>
        

         
            <section class="tm-welcome-section" style="padding-top:  10px;">
               <h1 style="color: white;padding-top: 30px; ">FOOD CATEGORY</h1>
               <div class="container" style="text-align: center; padding-left:200px; color:black">
               
                <?php
                $sql1="select * from `tbl_category`";
                $res1=mysqli_query($con,$sql1);
                echo "<table class='table table-responsive' id='tbl' class='table'style='display:block; padding:50px; color:white; font-size:20px;'>";
                echo "<tr>";
                echo"<th> FOOD CATEGORY ID</th>";
                echo"<th> FOOD CATEGORY NAME</th>";
                echo"</tr>";
                while($row=mysqli_fetch_array($res1))
                {
                echo"<tr >";
                echo"<td>",$row['cid'],"</td><td>&nbsp;",$row['cname'],"</td>";
                echo"</tr>";
                }
                echo"  </table>";
                ?>
              </div>

            </section>
        
  </div>
  <section></section>
    <footer>
        <div class="container">
          <div class="row tm-copyright">
           <p class="col-lg-12 small copyright-text text-center">Copyright &copy; 2084 Your Cafe House</p>
         </div>  
       </div>
     </div>
   </footer> <!-- Footer content-->  
   <!-- JS -->
   <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>      <!-- jQuery -->
   <script type="text/javascript" src="js/templatemo-script.js"></script>      <!-- Templatemo Script -->

 </body>
 </html>
